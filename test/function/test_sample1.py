import sys
import os
import unittest
sys.path.append(os.path.join(os.path.dirname(__file__), '../../src/'))
import sample1

class CalcTest(unittest.TestCase):
    def setUp(self):
        # initialize
        pass
    
    def tearDown(self):
        # finish
        pass
    
    def test_normal(self):
        self.assertEqual(2, sample1.sample1(1, 2))
        
if __name__ == "__main__":
    unittest.main()