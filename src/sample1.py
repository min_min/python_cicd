# coding:utf-8
def sample1(number1, number2):
    return number1 * number2


if __name__ == "__main__":
    for i in range(1, 10):
        print(sample1(i, i + 2))
